//svg
(function(f,l,m,e,g,n){function h(a,b){if(b){var c=b.getAttribute("viewBox"),d=f.createDocumentFragment(),e=b.cloneNode(!0);for(c&&a.setAttribute("viewBox",c);e.childNodes.length;)d.appendChild(e.childNodes[0]);a.appendChild(d)}}function p(){var a=f.createElement("x"),b=this.s;a.innerHTML=this.responseText;this.onload=function(){b.splice(0).map(function(b){h(b[0],a.querySelector("#"+b[1].replace(/(\W)/g,"\\$1")))})};this.onload()}function k(){for(var a;a=l[0];)if(g){var b=new Image;b.src=a.getAttribute("xlink:href").replace("#",
".").replace(/^\./,"")+".png";a.parentNode.replaceChild(b,a)}else{var b=a.parentNode,c=a.getAttribute("xlink:href").split("#"),d=c[0],c=c[1];b.removeChild(a);if(d.length){if(a=e[d]=e[d]||new XMLHttpRequest,a.s||(a.s=[],a.open("GET",d),a.onload=p,a.send()),a.s.push([b,c]),4===a.readyState)a.onload()}else h(b,f.getElementById(c))}m(k)}(g||n)&&k()})(document,document.getElementsByTagName("use"),window.requestAnimationFrame||window.setTimeout,{},/MSIE\s[1-8]\b/.test(navigator.userAgent),/Trident\/[567]\b/.test(navigator.userAgent)||
537>(navigator.userAgent.match(/AppleWebKit\/(\d+)/)||[])[1],document.createElement("svg"),document.createElement("use"));

/* Ресайз карты на странице support */

if($(window).width() >= 992) {
    var height = $(document).height() - $('header').height() - $('.block').height() - $('footer').height();
    var width = (($('body').width() - 1200)/2)+(1200 * 0.5) - 15;
    $('.map').css({ 'height': height, 'width': width });
} else {
    $('.support .map').each(function(i, e) {
        var w_post_img = $(e).width();
        var h_post_img = (w_post_img * 3) / 4;
        $(e).css('height', h_post_img);
    });
}
$(window).resize(function() {
    if($(window).width() >= 992) {
        var height = $(document).height() - $('header').height() - $('.block').height() - $('footer').height();
        var width = (($('body').width() - 1200)/2)+(1200 * 0.5) - 15;
        $('.map').css({ 'height': height, 'width': width });
    } else {
        $('.support .map').each(function(i, e) {
            var w_post_img = $(e).width();
            var h_post_img = (w_post_img * 3) / 4;
            $(e).css('height', h_post_img);
        });
    }
});

$('.invest_tabs .tab').click(function() {
    $('.invest_tabs .tab').removeClass('active');
    $(this).addClass('active');
    var id = $(this).attr('id');
    $('.inv').fadeOut( 0, function() { });
    $('.inv.id_' + id).fadeIn( 600, function() { });
});

/* Копирование ссылки при клике на кнопку */

$('#copybtn').click(function () {
    copy();
});

function copy() {
    var cutTextarea = document.querySelector('#copy');
    cutTextarea.select();
    try {
        var successful = document.execCommand('copy');
        var msg = successful ? 'successful' : 'unsuccessful';
        console.log('Copy');
    } catch (err) {
        console.log('Oops, unable to cut');
    }
}

/* Вопроизведение анимации chartist при скроллинге до элемента chart */

var toCenter = 0,
    toBottom = 0,
    i = 0;

$(document).ready(function(){
    a = $('#chart1');
    b = $('#chart2');
    c = $('#chart3');

    poh = document.getElementById('chartist');
    function showRect(elem) {
        var r = elem.getBoundingClientRect(),
            f = document.documentElement.clientHeight;
        toBottom      = r.bottom;
        toCenter      = f/2;
        // console.log(toBottom, toCenter);
    }

    window.onscroll = function () {
        showRect(poh);
        console.log(i, 'val');
        if ( toBottom <= toCenter && i <= 2 ) {
            chartists_wrapper(a);
            chartists_wrapper(b);
            chartists_wrapper(c);
        } else {
            return;
        }
    };
});

function chartists_wrapper(a) {
    var ch = a.find('b').data('chartsymbol');
    a.find('b').html(ch + '%');

    // alert(ch);

    var chart = new Chartist.Pie(a[0], {
        series: [ch, 1]
    }, {
        donut: true,
        donutWidth: 4,
        showLabel: false
    });

    chart.on('draw', function(data) {
        if(data.type === 'slice') {
            // Get the total path length in order to use for dash array animation
            var pathLength = data.element._node.getTotalLength();

            // Set a dasharray that matches the path length as prerequisite to animate dashoffset
            data.element.attr({
                'stroke-dasharray': pathLength + 'px ' + pathLength + 'px'
            });

            // Create animation definition while also assigning an ID to the animation for later sync usage
            var animationDefinition = {
                'stroke-dashoffset': {
                    id: 'anim' + data.index,
                    dur: 2000,
                    from: -pathLength + 'px',
                    to:  '0px',
                    easing: Chartist.Svg.Easing.easeOutQuint,
                    // We need to use `fill: 'freeze'` otherwise our animation will fall back to initial (not visible)
                    fill: 'freeze'
                }
            };

            // If this was not the first slice, we need to time the animation so that it uses the end sync event of the previous animation
            if(data.index !== 0) {
                animationDefinition['stroke-dashoffset'].begin = 'anim' + (data.index - 1) + '.end';
            }

            // We need to set an initial value before the animation starts as we are not in guided mode which would do that for us
            data.element.attr({
                'stroke-dashoffset': -pathLength + 'px'
            });

            // We can't use guided mode as the animations need to rely on setting begin manually
            // See http://gionkunz.github.io/chartist-js/api-documentation.html#chartistsvg-function-animate
            data.element.animate(animationDefinition, false);
        }
    });

    // For the sake of the example we update the chart every time it's created with a delay of 8 seconds
    chart.on('created', function() {
        if(window.__anim21278907124) {
            clearTimeout(window.__anim21278907124);
            window.__anim21278907124 = null;
        }
        window.__anim21278907124 = setTimeout(chart.update.bind(chart), 100000);
    });
    i++;
};

/* Вопроизведение анимации chartist без скроллинга на страницах not_home */

$(document).ready(function() {
    var ch4 = $('#chart4').find('b').data('chartsymbol');
    $('#chart4').find('b').html(ch4 + '%');

    var chart = new Chartist.Pie('#chart4', {
        series: [ch4, 1]
    }, {
        donut: true,
        donutWidth: 4,
        showLabel: false
    });

    chart.on('draw', function (data) {
        if (data.type === 'slice') {
            // Get the total path length in order to use for dash array animation
            var pathLength = data.element._node.getTotalLength();

            // Set a dasharray that matches the path length as prerequisite to animate dashoffset
            data.element.attr({
                'stroke-dasharray': pathLength + 'px ' + pathLength + 'px'
            });

            // Create animation definition while also assigning an ID to the animation for later sync usage
            var animationDefinition = {
                'stroke-dashoffset': {
                    id: 'anim' + data.index,
                    dur: 2000,
                    from: -pathLength + 'px',
                    to: '0px',
                    easing: Chartist.Svg.Easing.easeOutQuint,
                    // We need to use `fill: 'freeze'` otherwise our animation will fall back to initial (not visible)
                    fill: 'freeze'
                }
            };

            // If this was not the first slice, we need to time the animation so that it uses the end sync event of the previous animation
            if (data.index !== 0) {
                animationDefinition['stroke-dashoffset'].begin = 'anim' + (data.index - 1) + '.end';
            }

            // We need to set an initial value before the animation starts as we are not in guided mode which would do that for us
            data.element.attr({
                'stroke-dashoffset': -pathLength + 'px'
            });

            // We can't use guided mode as the animations need to rely on setting begin manually
            // See http://gionkunz.github.io/chartist-js/api-documentation.html#chartistsvg-function-animate
            data.element.animate(animationDefinition, false);
        }
    });

// For the sake of the example we update the chart every time it's created with a delay of 8 seconds
    chart.on('created', function () {
        if (window.__anim21278907124) {
            clearTimeout(window.__anim21278907124);
            window.__anim21278907124 = null;
        }
        window.__anim21278907124 = setTimeout(chart.update.bind(chart), 100000);
    });
});

$(document).ready(function() {
    var ch5 = $('#chart5').find('b').data('chartsymbol');
    $('#chart5').find('b').html(ch5 + '%');

    var chart = new Chartist.Pie('#chart5', {
        series: [ch5, 1]
    }, {
        donut: true,
        donutWidth: 4,
        showLabel: false
    });

    chart.on('draw', function (data) {
        if (data.type === 'slice') {
            // Get the total path length in order to use for dash array animation
            var pathLength = data.element._node.getTotalLength();

            // Set a dasharray that matches the path length as prerequisite to animate dashoffset
            data.element.attr({
                'stroke-dasharray': pathLength + 'px ' + pathLength + 'px'
            });

            // Create animation definition while also assigning an ID to the animation for later sync usage
            var animationDefinition = {
                'stroke-dashoffset': {
                    id: 'anim' + data.index,
                    dur: 2000,
                    from: -pathLength + 'px',
                    to: '0px',
                    easing: Chartist.Svg.Easing.easeOutQuint,
                    // We need to use `fill: 'freeze'` otherwise our animation will fall back to initial (not visible)
                    fill: 'freeze'
                }
            };

            // If this was not the first slice, we need to time the animation so that it uses the end sync event of the previous animation
            if (data.index !== 0) {
                animationDefinition['stroke-dashoffset'].begin = 'anim' + (data.index - 1) + '.end';
            }

            // We need to set an initial value before the animation starts as we are not in guided mode which would do that for us
            data.element.attr({
                'stroke-dashoffset': -pathLength + 'px'
            });

            // We can't use guided mode as the animations need to rely on setting begin manually
            // See http://gionkunz.github.io/chartist-js/api-documentation.html#chartistsvg-function-animate
            data.element.animate(animationDefinition, false);
        }
    });

// For the sake of the example we update the chart every time it's created with a delay of 8 seconds
    chart.on('created', function () {
        if (window.__anim21278907124) {
            clearTimeout(window.__anim21278907124);
            window.__anim21278907124 = null;
        }
        window.__anim21278907124 = setTimeout(chart.update.bind(chart), 100000);
    });
});

$(document).ready(function() {
    var ch6 = $('#chart6').find('b').data('chartsymbol');
    $('#chart6').find('b').html(ch6 + '%');

    var chart = new Chartist.Pie('#chart6', {
        series: [ch6, 1]
    }, {
        donut: true,
        donutWidth: 4,
        showLabel: false
    });

    chart.on('draw', function (data) {
        if (data.type === 'slice') {
            // Get the total path length in order to use for dash array animation
            var pathLength = data.element._node.getTotalLength();

            // Set a dasharray that matches the path length as prerequisite to animate dashoffset
            data.element.attr({
                'stroke-dasharray': pathLength + 'px ' + pathLength + 'px'
            });

            // Create animation definition while also assigning an ID to the animation for later sync usage
            var animationDefinition = {
                'stroke-dashoffset': {
                    id: 'anim' + data.index,
                    dur: 2000,
                    from: -pathLength + 'px',
                    to: '0px',
                    easing: Chartist.Svg.Easing.easeOutQuint,
                    // We need to use `fill: 'freeze'` otherwise our animation will fall back to initial (not visible)
                    fill: 'freeze'
                }
            };

            // If this was not the first slice, we need to time the animation so that it uses the end sync event of the previous animation
            if (data.index !== 0) {
                animationDefinition['stroke-dashoffset'].begin = 'anim' + (data.index - 1) + '.end';
            }

            // We need to set an initial value before the animation starts as we are not in guided mode which would do that for us
            data.element.attr({
                'stroke-dashoffset': -pathLength + 'px'
            });

            // We can't use guided mode as the animations need to rely on setting begin manually
            // See http://gionkunz.github.io/chartist-js/api-documentation.html#chartistsvg-function-animate
            data.element.animate(animationDefinition, false);
        }
    });

// For the sake of the example we update the chart every time it's created with a delay of 8 seconds
    chart.on('created', function () {
        if (window.__anim21278907124) {
            clearTimeout(window.__anim21278907124);
            window.__anim21278907124 = null;
        }
        window.__anim21278907124 = setTimeout(chart.update.bind(chart), 100000);
    });
});

/* Инициализация WOW эффектов */

new WOW().init();

/* Создание калькулятора */

$(document).ready(function () {
    qwer();
});

$("input.dataInput").on('input', function () {
    di = $(this).val();
    parseFloat(di);
    if (document.getElementsByClassName('range01').length > 0) {
        $(".range01").slider("option", "values", [0, di]);
    }

    if (di >= +summin && di < sumsecond) {
        percent = percentfirst;
    } else if (di >= +sumsecond && di <= sumthird) {
        percent = percentsecond;
    } else if (di >= +sumthird && di < summax) {
        percent = percentthird;
    } else {
        percent = percentlast;
    }

    qwer();
});

$("input.daysInput").on('input', function () {
    days = $(this).val();
    parseInt(days);

    if (di >= +summin && di < sumsecond) {
        percent = percentfirst;
    } else if (di >= +sumsecond && di <= sumthird) {
        percent = percentsecond;
    } else if (di >= +sumthird && di < summax) {
        percent = percentthird;
    } else {
        percent = percentlast;
    }

    qwer();
});

function qwer() {
    di = $('input.dataInput').val();
    days = $('input.daysInput').val();

    var per        = percent * days;
    profit     = di * per / 100;
    hashpower  = profit * rate;

    $('.percent').html(per.toFixed(2) + '% of investment');
    $('.profit').html(+profit.toFixed(2) + ' USD');
    $('.hashpower').html(+hashpower.toFixed(2) + ' GH/s');
    $('.days').html(+days);
};

if (document.getElementsByClassName('range01').length > 0) { // те такие блоки есть
    function range(min, max) {
        $(".range01").slider({
            range: true,
            min: min,
            max: max,
            disabled: true
        });
    }
    range(summin, summax);
}